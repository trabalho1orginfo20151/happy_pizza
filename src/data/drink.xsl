<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body>
        <table border="1">
          <tr>
            <th>Sabores</th>
            <th>Refil</th>
            <th>Unidade</th>
          </tr>
          <xsl:for-each select="pizzaria/drinks/drink">
            <tr>
              <td><xsl:value-of select="name"/></td>
              <td><xsl:value-of select="refill"/></td>
              <td><xsl:value-of select="unit"/></td>
            </tr>
          </xsl:for-each>
        </table>

      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
